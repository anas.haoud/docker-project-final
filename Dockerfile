# Utilisez une image de base Node.js, par exemple la version 16
FROM node:20

# Créer un répertoire de travail dans l'image
WORKDIR /usr/src/app

# Copier les fichiers package.json et package-lock.json (si existant)
COPY package*.json ./

# Installer les dépendances du projet
RUN npm install

RUN npm install express

# Reconstruire les modules natifs pour l'environnement actuel
RUN npm rebuild bcrypt --build-from-source

# Copier le reste des fichiers de votre projet dans le répertoire de travail
COPY . .

# Exposer le port sur lequel votre serveur Node.js s'exécutera
EXPOSE 3000

# Définir la commande pour démarrer votre serveur
CMD ["node", "server.js"]
