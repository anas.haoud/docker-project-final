CREATE TABLE IF NOT EXISTS registration_form (
    id_user SERIAL PRIMARY KEY,
    username VARCHAR(50) UNIQUE NOT NULL,
    user_mail VARCHAR(100) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL
);
