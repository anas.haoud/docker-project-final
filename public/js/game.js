class Letter {
    constructor(data) {
        this.letter = data;
        this.guessed = false;
    }

    display() {
        return this.guessed ? this.letter : "_";
    }

    check(guess) {
        if (guess === this.letter) {
            this.guessed = true;
        }
        return this.guessed;
    }
}

class Word {
    constructor(word, clue) {
        this.letters = word.split("").map(char => new Letter(char));
        this.clue = clue;
    }

    displayWord() {
        return this.letters.map(letter => letter.display()).join(" ");
    }

    displayClue() {
        return this.clue;
    }

    guessLetter(guess) {
        this.letters.forEach(letter => letter.check(guess));
    }

    isComplete() {
        return this.letters.every(letter => letter.guessed);
    }
}

let wordsArray = [];
let gameWord;
let counter = 10;

// Récupérer les mots de la base de données
function getWordsFromDB() {
    fetch('/get-words')
        .then(response => response.json())
        .then(data => {
            wordsArray = data; // Stocker l'ensemble des mots et énigmes
            startGame(); // Démarrer le jeu une fois les mots chargés
        })
        .catch(error => console.error('Error:', error));
}

function startGame() {
    if (wordsArray.length === 0) {
        alert("Loading words from the database...");
        getWordsFromDB();
        return;
    }
    const randomNumber = Math.floor(Math.random() * wordsArray.length);
    const selectedWord = wordsArray[randomNumber];
    gameWord = new Word(selectedWord.word.toLowerCase(), selectedWord.clue);
    updateDisplay();
}

function guessLetter() {
    const guess = document.getElementById('userInput').value.toLowerCase();
    document.getElementById('userInput').value = '';
    gameWord.guessLetter(guess);
    if (gameWord.letters.some(letter => letter.letter === guess)) {
        if (gameWord.isComplete()) {
            updateDisplay();
            alert("You won! Starting a new game.");
            counter = 10;
            startGame();
        } else {
            updateDisplay();
        }
    } else {
        counter--;
        alert("Sorry, wrong letter! You have " + counter + " tries remaining.");
        if (counter <= 0) {
            alert("Sorry, you've run out of turns! Let's play again.");
            startGame();
        }
    }
}

function updateDisplay() {
    document.getElementById('wordDisplay').textContent = gameWord.displayWord();
    document.getElementById('clueDisplay').textContent = "Clue: " + gameWord.displayClue();
    // Mettre à jour les indices restants ou d'autres informations sur le jeu
}

getWordsFromDB(); // Charger les mots de la base de données au démarrage
