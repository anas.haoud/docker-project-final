var registrationForm = document.getElementById('registrationForm');
if (registrationForm) {
    registrationForm.addEventListener('submit', function(event){
        event.preventDefault();

        var username = document.getElementById('username').value;
        var email = document.getElementById('email').value;
        var password = document.getElementById('password').value;
        var errorMessage = document.getElementById('errorMessage');

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/register', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 201) {
                    alert("Registration successful!");
                    window.location.href = 'connexionForm.html'; // Redirection vers la page de connexion
                } else if (xhr.status === 409) {
                    errorMessage.textContent = 'Username or email already exists';
                } else {
                    errorMessage.textContent = 'An error occurred during registration';
                }
            }
        };
        xhr.send(JSON.stringify({
            username: username,
            email: email,
            password: password
        }));
    });
}


var loginForm = document.getElementById('loginForm');
if (loginForm) {
    loginForm.addEventListener('submit', function(event){
        event.preventDefault();

        var email = document.getElementById('email').value;
        var password = document.getElementById('password').value;
        var loginErrorMessage = document.getElementById('loginErrorMessage');

        loginErrorMessage.textContent = '';

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/login', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    window.location.href = '/welcome.html';
                    // Gérer une connexion réussie ici
                } else {
                    // Afficher un message d'erreur en cas d'échec de la connexion
                    loginErrorMessage.textContent = 'Invalid email or password';
                }
            }
        };
        xhr.send(JSON.stringify({
            email: email,
            password: password
        }));
    });
}