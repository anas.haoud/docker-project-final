const express = require('express');
const bodyParser = require('body-parser');
const { Pool } = require('pg');
const bcrypt = require('bcrypt');
const path = require('path');
const mongoose = require('mongoose');
const app = express();
const port = 3000;


const mongoDB = 'mongodb://mongo:27017/docker_test'; // Utilisez le nom de service "mongo"
mongoose.connect(mongoDB);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

/*
// Configuration de MongoDB
const mongoDB = 'mongodb://localhost:27017/docker_test';
mongoose.connect(mongoDB);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
*/


// Schéma Mongoose pour les compteurs
const Schema = mongoose.Schema;
const CounterSchema = new Schema({
    _id: { type: String, required: true },
    seq: { type: Number, default: 0 }
});
const counter = mongoose.model('counter', CounterSchema);

// Fonction pour obtenir le prochain ID
async function getNextSequence(name) {
    const ret = await counter.findOneAndUpdate(
        { _id: name },
        { $inc: { seq: 1 } },
        { new: true, upsert: true }
    );
    return ret.seq;
}

// Schéma Mongoose pour les utilisateurs
const UserSchema = new Schema({
    id_user: Number,
    username: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true }
});
const UserModel = mongoose.model('User', UserSchema, 'registration_form');


// Schéma Mongoose pour les mots du jeu
const WordSchema = new mongoose.Schema({
    word: String,
    clue: String  // Ajout d'un champ pour l'énigme
});
const WordModel = mongoose.model('Word', WordSchema);



async function initializeWordsCollection() {
    const wordsCount = await WordModel.countDocuments();

    if (wordsCount === 0) {
        // Liste des mots à ajouter
        const defaultWords = ["Butterfly", "Lady Bug", "Car", "Star Wars", "Bookcase", "Superman and Batman", "Election", "Apple", "Pepsi Cola", "Alligator", "Television Show", "Bicycle", "Laptop", "Masters Degree"];

        // Ajouter chaque mot dans la base de données
        await Promise.all(defaultWords.map(word => {
            const wordDoc = new WordModel({ word });
            return wordDoc.save();
        }));

        console.log("Words collection initialized with default words.");
    }
}

async function initializeWordsCollection() {
    const wordsCount = await WordModel.countDocuments();

    if (wordsCount === 0) {
        // Liste des mots et énigmes à ajouter
        const defaultWords = [
            { word: "Algorithme", clue: "Je suis une recette dans le monde du code, guidant chaque étape pour atteindre un but précis. Qui suis-je ?" },
            { word: "Variable", clue: "Je change et évolue avec le temps, portant des informations qui ne sont jamais permanentes. Dans le monde du code, je suis essentiel. Qui suis-je ?" },
            { word: "Boucle", clue: "Je tourne en rond sans jamais m'arrêter, à moins qu'on me dise de le faire. J'aide à répéter les tâches avec facilité. Qui suis-je ?" },
            { word: "Fonction", clue: "J'ai un nom, j'agis quand on m'appelle et je réalise une tâche spécifique. Dans le code, je suis un outil de modularité. Qui suis-je ?" },
            { word: "Bug", clue: "Je suis l'intrus indésirable, le fauteur de troubles dans le monde numérique. Je provoque confusion et frustration, mais je suis aussi un appel à la correction. Qui suis-je ?" },
            { word: "Base de données", clue: "Je suis le gardien des secrets, le stockeur d'informations. Je suis organisé, méthodique et essentiel pour la mémoire numérique. Qui suis-je ?" },
            { word: "HTML", clue: "Je structure et je forme, créant des fondations visibles sur le web. Sans moi, les pages seraient sans forme et chaos. Qui suis-je ?" },
            { word: "Intelligence Artificielle", clue: "Née de l'esprit humain, je simule l'intelligence et l'apprentissage. Je grandis à travers des algorithmes et des données, me rapprochant toujours plus de la création de mon créateur. Qui suis-je ?" },
            { word: "Cryptographie", clue: "Je suis l'art de la dissimulation et de la protection. Dans le monde numérique, je garde les secrets bien cachés. Qui suis-je ?" },
            { word: "Réseau", clue: "Je suis une toile interconnectée, un lien invisible qui unit le monde numérique. Sans moi, la communication serait isolée et limitée. Qui suis-je ?" },
            { word: "Python", clue: "Je suis à la fois un serpent et un langage, connu pour ma simplicité et ma flexibilité. Dans le monde du code, je suis aimé pour ma clarté. Qui suis-je ?" },
            { word: "Java", clue: "Je suis une île, mais aussi un langage. Robuste et omniprésent, je fonctionne sur des millions d'appareils à travers le monde. Qui suis-je ?" },
            { word: "C#", clue: "Je suis une note de musique, mais aussi un langage, le choix préféré pour les applications de type entreprise et les jeux vidéo. Qui suis-je ?" },
            { word: "Ruby", clue: "Je suis une pierre précieuse dans le monde réel et un langage de programmation aimé pour ma beauté et ma simplicité dans le monde numérique. Qui suis-je ?" },
            { word: "JavaScript", clue: "Je suis le sculpteur du web, donnant vie aux pages avec interactivité et animation. Sans moi, le web serait bien plus statique. Qui suis-je ?" },
            { word: "SQL", clue: "Je suis le langage des questions, spécialisé dans l'extraction d'informations précieuses des profondeurs des bases de données. Qui suis-je ?" },
            { word: "Swift", clue: "Rapide et agile, je suis le choix moderne pour créer des applications sur une certaine plateforme de fruits. Qui suis-je ?" },
            { word: "PHP", clue: "Je fonctionne en coulisses sur le serveur, jouant un rôle clé dans la création de sites web dynamiques. Qui suis-je ?" },
            { word: "Go", clue: "Je suis simple et efficace, conçu pour la performance et la concurrence. Mon nom est court, mais dans le monde de la programmation, je vais loin. Qui suis-je ?" },
            { word: "Rust", clue: "Je suis à la fois un phénomène naturel et un langage de programmation, réputé pour ma sécurité et ma performance. Qui suis-je ?" }
        ];

        // Ajouter chaque mot et énigme dans la base de données
        await Promise.all(defaultWords.map(({ word, clue }) => {
            const wordDoc = new WordModel({ word, clue });
            return wordDoc.save();
        }));

        console.log("Words collection initialized with default words and clues.");
    }
}


// Appeler la fonction après la connexion à MongoDB
db.once('open', initializeWordsCollection);



const pool = new Pool({
    user: 'postgres',
    host: 'db', // Utilisez le nom de service "db"
    database: 'docker_test',
    password: 'zebraanas',
    port: 5432,
});

/*
// Configuration de PostgreSQL
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'docker_test',
    password: 'zebraanas',
    port: 5432,
});
*/


app.use(bodyParser.json());

// Servir les fichiers statiques
app.use(express.static('public'));

// Route pour le formulaire d'inscription
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Route d'inscription (POST)
app.post('/register', async (req, res) => {
    const { username, email, password } = req.body;

    try {
        // Vérifier si l'utilisateur existe déjà dans PostgreSQL
        const checkUser = 'SELECT * FROM registration_form WHERE username = $1 OR user_mail = $2';
        const checkResult = await pool.query(checkUser, [username, email]);

        if (checkResult.rows.length > 0) {
            return res.status(409).send('Username or email already exists');
        }

        // Hasher le mot de passe
        const hashedPassword = await bcrypt.hash(password, 10);

        // Obtenir le prochain ID utilisateur pour MongoDB
        const id_user = await getNextSequence('user_id');

        // Insérer dans PostgreSQL
        const insertQuery = 'INSERT INTO registration_form (username, user_mail, password) VALUES ($1, $2, $3) RETURNING id_user;';
        const values = [username, email, hashedPassword];
        await pool.query(insertQuery, values);

        // Insérer dans MongoDB
        const mongoUser = new UserModel({ id_user, username, email, password: hashedPassword });
        await mongoUser.save();

        res.status(201).send('User registered successfully');
    } catch (err) {
        console.error('Error executing query', err.stack);
        res.status(500).send('An error occurred while registering the user');
    }
});

// Route de connexion (POST)
app.post('/login', async (req, res) => {
    const { email, password } = req.body;

    try {
        // Vérifier dans PostgreSQL
        const query = 'SELECT * FROM registration_form WHERE user_mail = $1';
        const result = await pool.query(query, [email]);

        if (result.rows.length > 0) {
            const user = result.rows[0];

            if (bcrypt.compareSync(password, user.password)) {
                return res.status(200).send('Login successful');
            }
        }
        else{
            // Vérifier dans MongoDB
            UserModel.findOne({ email: email }, (err, userMongo) => {
                if (err) {
                    console.error('Error accessing MongoDB', err);
                    return res.status(500).send('An error occurred during login');
                }

                if (userMongo && bcrypt.compareSync(password, userMongo.password)) {
                    return res.status(200).send('Login successful');
                }

                res.status(401).send('Invalid email or password');
            });
        }
        
    } catch (err) {
        console.error('Error executing query', err.stack);
        res.status(500).send('An error occurred during login');
    }
});


// Route pour ajouter un mot avec une énigme
app.post('/add-word', async (req, res) => {
    const { word, clue } = req.body;  // Récupérer également l'énigme
    const newWord = new WordModel({ word, clue });
    try {
        await newWord.save();
        res.status(201).send('Word and clue added successfully');
    } catch (error) {
        res.status(500).send('Error adding word and clue');
    }
});


// Route pour obtenir tous les mots
app.get('/get-words', async (req, res) => {
    try {
        const words = await WordModel.find();
        res.status(200).json(words);
    } catch (error) {
        res.status(500).send('Error fetching words');
    }
});


// Démarrer le serveur
app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
